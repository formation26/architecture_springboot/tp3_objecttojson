package fr.epita.tp3.application;

import org.springframework.stereotype.Service;

import fr.epita.tp3.entite.Livre;

@Service
public class LivreServiceImpl implements LivreService {

	@Override
	public Livre getLivre() {

		Livre l=new Livre();
		l.setNbrePage(2598);
		l.setNomAuteur("Victor Hugo");
		l.setTitre("Les misérables");
		return l;
	}

}
