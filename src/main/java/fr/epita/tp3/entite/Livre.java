package fr.epita.tp3.entite;

public class Livre {
	
	private String nomAuteur;
	private String titre;
	private int nbrePage;
	
	
	public String getNomAuteur() {
		return nomAuteur;
	}
	public void setNomAuteur(String nomAuteur) {
		this.nomAuteur = nomAuteur;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public int getNbrePage() {
		return nbrePage;
	}
	public void setNbrePage(int nbrePage) {
		this.nbrePage = nbrePage;
	}
	
	

}
