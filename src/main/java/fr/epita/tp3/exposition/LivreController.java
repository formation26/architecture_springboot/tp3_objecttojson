package fr.epita.tp3.exposition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.epita.tp3.application.LivreService;
import fr.epita.tp3.entite.Livre;

@RestController
@RequestMapping("/livre")
public class LivreController {
   
	@Autowired
	LivreService service;
	
	@GetMapping
	public Livre getLivre() {
		
		return service.getLivre();
	}
	
}
